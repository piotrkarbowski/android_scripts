#!/system/bin/sh
# CyanogenMod 10.1 does not have option to disable camera sound
# which alone is extreme annoying.

if [ "$(whoami)" != 'root' ]; then
	exec su -c "sh $0"
fi
if [ -f /system/media/audio/ui/camera_click.ogg ]; then
	set -ex
	mount -o remount,rw /system
	rm /system/media/audio/ui/camera_click.ogg
	mount -o remount,ro /system
else
	echo "camera_clock.ogg already removed."
fi
