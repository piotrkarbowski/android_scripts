#!/system/bin/sh
# Link /etc/hosts to /storage/sdcard0/hosts which suppose to have adblock domains.
if [ "$(whoami)" != 'root' ]; then
	exec su -c "sh $0"
fi

if ! [ -L /system/etc/hosts ]; then
	set -ex
	mount -o remount,rw /system
	rm /system/etc/hosts
	ln -s /storage/sdcard0/hosts /system/etc/hosts
	mount -o remount,ro /system
else
	echo "/etc/hosts is already a symlink."
fi
